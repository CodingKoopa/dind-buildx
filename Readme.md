# dind-buildx: Docker in Docker, with [buildx](https://docs.docker.com/buildx/working-with-buildx/)
The official [Docker in Docker](https://hub.docker.com/_/docker) image is useful for building Docker images in a Continuous Integration pipeline. It provides a Docker daemon with its `dind` tag and the expected Docker CLI elsewhere. However, [it does not include the buildx CLI plugin](https://github.com/docker-library/docker/issues/156), meaning you can't leverage the benefits of BuildKit, such as easily building images for multiple architectures.

This repository provides a Docker in Docker distribution with buildx support, as well as nightly builds for `linux/amd64`. As is the case for the upstream image, dind-buildx uses [Alpine Linux](https://www.alpinelinux.org/).


## Usage
dind-buildx's [Continuous Integration (CI)](https://docs.gitlab.com/ee/ci/) provides prebuilt images, updated when there is a new commit, as well as on a nightly basis for updates.

The images are pushed to [Docker Hub](https://hub.docker.com/r/yamdi/dind-buildx), with the following tags:

| Tags                               | Description                             | Entrypoint | `docker` Equivalent Tag |
| ---------------------------------- | --------------------------------------- | ---------- | ----------------------- |
| `latest`, `latest-cli`, `docker`   | Provides the binary for the Docker CLI. | `sh`       | `latest`                |
| `latest-daemon`, `dockerd`, `dind` | Runs the Docker daemon.                 | `dockerd`  | `dind`                  |

All of the tags above also have a variant with the date appended in `-YYYYMMDD` format. For instance, for the `latest-daemon` tag, there is a `latest-daemon-20210629` tag, and a `latest-daemon-20210630` tag, and so on.

### Usage with GitLab CI
dind-buildx works great with GitLab CI! You can use both the provided `docker` and `dockerd` images like so:
```yaml
build:
  image: yamdi/dind-buildx:docker
  services:
    - name: yamdi/dind-buildx:dockerd
      alias: docker
```
The `alias:` line is necessary so that the service will be available under the "docker" hostname.

For a simple example, see [alpine-docker-images](https://gitlab.com/CodingKoopa/alpine-docker-images). For a more complex example with caching and tagging by date, see [YAMDI](https://gitlab.com/CodingKoopa/yamdi).

buildx is a CLI plugin, meaning it only needs to be present where the `docker` CLI is running. As it deals with the interface rather than the engine internals, the `dockerd` image (that is, `dind-buildx:dockerd`) is largely the same as the upstream `dind` image (`docker:dind`). Two differences not directly related to buildx are:
- The `dind-buildx` image is updated every night. The upstream `docker` image is updated every release. Both images have the same buildx release, but possibly different system packages.
- `dind-buildx` includes a solution for [the MTU issue](#the-mtu-issue).

To fix the MTU issue while still using the `docker:dind` image, invoke `dockerd` like so:
```yaml
build:
  image: yamdi/dind-buildx:docker
  services:
    - name: docker:dind
      command: ["--mtu=1460"]
```

### Usage with other CI
Firstly, you should make sure you can use the upstream `docker` and `docker:dind` images in your CI, as they have some special requirements that other Docker images do not. Please refer to your CI documentation for more info.

Now, if those work, then this image will too, but you should understand [the MTU issue](#the-mtu-issue) first. By default, dind-buildx applies an MTU of `1460`, which is what the GitLab.com runner host network interfaces use. You may need to run `ip addr` and/or `ifconfig` to find out the MTU that your CI machines use. If it is different from `1460`, you **must** override the startup command, either to a blank set of arguments or to a different MTU, e.g. `--mtu=1500`. If your MTU is above the MTU of the host network, bad things can happen.

## The MTU Issue

### The Problem
Docker no longer attempts to use the [maximum transmission unit](https://en.wikipedia.org/wiki/Maximum_transmission_unit) specified by the host network interface when creating [bridge networks](https://docs.docker.com/network/bridge/). This was set into place by [this Moby pull request](https://github.com/moby/moby/pull/18108), which set the default MTU for newly created bridge networks to `1500`.

If the underlying network only supports network-layer protocol data units (PDUs) with a lower MTU, then [this will cause connectivity issues](https://sylwit.medium.com/how-we-spent-a-full-day-figuring-out-a-mtu-issue-with-docker-4d81fdfe2caf). Some other independent confirmations of this are:
- https://www.civo.com/learn/fixing-networking-for-docker
- https://mlohr.com/docker-mtu/
- http://atodorov.org/blog/2017/12/08/how-to-configure-mtu-for-the-docker-network/

What makes this issue not materialize all the time is, on your home network where you may be using Docker some of the time, an MTU of `1500` is probably just fine. Some more specialized networks, however, may have additional headers in their network-layer PDUs. For instance, the Google Cloud VPN specifications [require an MTU no greater than `1460`](https://cloud.google.com/network-connectivity/docs/vpn/concepts/mtu-considerations#gateway_mtu_versus_system_mtu).

The story doesn't end here, though - when there is an MTU conflict, networking code doesn't just throw its hands up in the air and give up. Networking devices can implement [Path MTU Discovery](https://en.wikipedia.org/wiki/Path_MTU_Discovery) (PMTUD), for negotiating MTU size using [Internet Control Message Protocol](https://en.wikipedia.org/wiki/Internet_Control_Message_Protocol) (ICMP) packets which incur an additional round trip. So, let's run through an example:
- A Docker container with a bridge network configured with the default settings (including an MTU of `1500`) needs to download a file, initializing a TCP session with a web server.
- The host, which happens to be configured with an MTU of `1460`, as its network has additional headers in its network-level PDU, propagates the TCP messages.
- The TCP session is successfully initialized, with a TCP maximum segment size (MSS) corresponding with an MTU of `1500`.
- The web server transmits a TCP segment with the requested MSS size.
- The router on the local network is unable to accept this PDU and must drop it. If configured for PMTUD, it will send an ICMP packet indicating that further fragmentation of the data is needed.
- The router on the remote network - or, perhaps a dedicated network security device - could indiscriminately drop all ICMP packets.

With that, this exchange is doomed. The Docker container MTU had a mismatch with the network MTU, and, due to the remote network's security policies, PMTUD could not be applied. So then, [the program might just hang](https://github.com/gliderlabs/docker-alpine/issues/307). [And perhaps eat away at your CI minutes](https://gitlab.com/CodingKoopa/zstd-rclone/-/jobs/1618738159).

### The Solution
Technically, PMTUD is the first solution to the MTU issue, but we've explained why that doesn't always work, and Wikipedia could give you even more reasons.

The other solution is specific to TCP communications: [TCP MSS clamping](https://docs.vmware.com/en/VMware-NSX-T-Data-Center/3.1/administration/GUID-5ADD0FBB-7F30-4933-8737-2AC0D919EE3F.html). MSS clamping, when implemented and enabled, is integrated into TCP connection establishment. While the session is being initialized, on the way to the remote device, the MSS is updated by the networking devices in the TCP header to accommodate known MTU limitations. The MSS is directly related to the MTU, offset by a constant value.

MSS clamping is a sweeping, effective solution. But maybe you don't have control over the network infrastructure. Then, you only really have one remaining solution: Fix the MTU mismatch manually.

The Docker daemon offers a command line flag (`--mtu $MTU`) and a configuration entry in `/etc/docker/daemon.json` (`mtu`) for setting the MTU for the default bridge. For custom networks, the MTU may be configured with the Docker CLI.

### Solutions for GitLab CI
GitLab CI appears to be impacted by this issue, though it's unclear exactly why. For [zstd-rclone](https://gitlab.com/CodingKoopa/zstd-rclone), the builds suddenly broke without any changes having been made to the Dockerfile or CI configuration. One possibility is the Alpine Linux CDN directing to different servers, some of which do support PMTUD.

To fix this, we pass `--mtu 1460` by default to `dockerd` to set the MTU for the bridge network to an [empirically determined value](https://gitlab.com/CodingKoopa/zstd-rclone/-/jobs/1623196574). So as long as that hasn't changed, you don't have to do anything!
